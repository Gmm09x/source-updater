import cores/config_utils
import cores/logging
import cores/utils
import cores/help
import jobs
import os
import strutils
import sequtils


type
  RunMode = enum
    mode_clone, mode_pull, mode_update
  ProgOptions = object
    run_mode: RunMode
    verbose: bool
    projects: seq[string]


proc updater_start_update(logger: LogHandler, pkg_name, pkg_path: string, show_verbose: bool, pull_only: bool) =
  var
    new_version: string

  if ujobs_pull(logger, pkg_name, pkg_path, show_verbose) != 0 or pull_only:
    return
  if ujobs_upgrade(logger, pkg_name, pkg_path, new_version, show_verbose) != 0:
    return
  if ujobs_changelog(logger, pkg_name, pkg_path, new_version, show_verbose) != 0 :
    return
  if ujobs_tag(logger, pkg_name, pkg_path, show_verbose) != 0:
    return
  if ujobs_push(logger, pkg_name, pkg_path, show_verbose) != 0:
    return


proc updater_handle_runmode(logger: LogHandler, run_mode: RunMode, git_url, pkg_name, root_path, project_path: string, verbose: bool) =
  case run_mode:
    of mode_pull:
      updater_start_update(logger, pkg_name, project_path, verbose, true)
    of mode_update:
      updater_start_update(logger, pkg_name, project_path, verbose, false)
    else:
      let repo_url = git_url & pkg_name
      discard ujobs_clone(logger, repo_url, pkg_name, root_path, verbose)


proc updater_handle_all_projects(run_mode: RunMode, configs: UpdateConfigs, verbose: bool) =
  let
    logger = ulog_init(configs.log_dir)

  for kind, path in walkDir(configs.root_dir):
    if kind == pcDir:
      let
        project_path = path
        project_name = path.splitPath.tail

      updater_handle_runmode(logger, run_mode, configs.git_url, project_name, configs.root_dir, project_path, verbose)


proc updater_handle_list_projects(run_mode: RunMode, configs: UpdateConfigs, list_projects: seq[string], verbose: bool) =
  let
    logger = ulog_init(configs.log_dir)

  for project_name in list_projects:
    let
      project_path = uutils_standardized_dirname(configs.root_dir & project_name)
    updater_handle_runmode(logger, run_mode, configs.git_url, project_name, configs.root_dir, project_path, verbose)


proc updater_get_args(): ProgOptions =
  var
    options: ProgOptions

  if paramCount() == 0:
    uupdate_short_help()
    quit(0)

  case paramStr(1):
    of "-h":
      uupdate_short_help()
      uupdate_full_help()
      quit(0)
    of "clone":
      options.run_mode = mode_clone
    of "pull":
      options.run_mode = mode_pull
    of "update":
      options.run_mode = mode_update
    else:
      uupdate_short_help()
      echo "Invalid run command"
      quit(1)

  if paramCount() >= 2:
    for each_args in commandLineParams()[1 .. ^1]:
      if each_args.startswith("-"):
        if each_args == "-h":
          uupdate_short_help()
          uupdate_full_help()
          quit(0)
        elif each_args == "-v":
          options.verbose = true
        # else: something else?
        # TODO add flag to allow customize config path
      else:
        options.projects.add(each_args)
    options.projects = deduplicate(options.projects)

  return options


proc update_start_program() =
  # TODO validate value of repo's url (http or git)
  let
    options = updater_get_args()
    configs = uconf_parse_config("config.json")

  uutils_check_required()
  if len(options.projects) == 0:
    updater_handle_all_projects(options.run_mode, configs, options.verbose)
  else:
    updater_handle_list_projects(options.run_mode, configs, options.projects, options.verbose)

update_start_program()
