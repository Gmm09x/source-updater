#[
  Parse configs and other things
]#

import json
import utils

type
  UpdateConfigs* = object
    root_dir*: string
    log_dir*: string
    git_url*: string


proc uconf_parse_config*(path: string): UpdateConfigs =
  var
    config: UpdateConfigs
  try:
    let
      json_data = parseFile(path)

    config.root_dir = uutils_standardized_dirname(json_data["root_path"].getStr())
    config.log_dir = uutils_standardized_dirname(json_data["log_path"].getStr())
    config.git_url = json_data["git_url"].getStr()

    return config
  except:
    echo getCurrentExceptionMsg()
    quit(1)
