#[
  Do useful stuff
]#

import strutils
import os


proc uutils_standardized_dirname*(name: string): string =
  #[
    Make sure dir name is correct and has no extra `/`
  ]#
  if not name.endswith("/"):
    return name & "/"
  return name


proc uutils_get_new_version_number*(txt: string): string =
  #[
    Parse output of uscan, get the latest version to make changelog
  ]#
  discard


proc uutils_check_required*() =
  if not fileExists("/usr/bin/git"):
    echo "git is missing"
    quit(1)
  if not fileExists("/usr/bin/gbp"):
    echo "git-buildpackage is missing"
    quit(1)
  if not fileExists("/usr/bin/uscan"):
    echo "uscan is missing"
    quit(1)


iterator uutils_lines*(text: string): string =
  var
    current_line = ""

  for each_char in text:
    current_line = current_line & $each_char
    if each_char == '\n':
      yield current_line
      current_line = ""
