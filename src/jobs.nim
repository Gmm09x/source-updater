#[
  Handle updates with commands
]#


import osproc
import cores/logging
import cores/utils
import strutils
import parseutils


proc ujobs_clone*(logger: LogHandler, git_url, pkg_name, work_dir: string, show_verbose: bool): int =
  var
    output: string

  ulog_info(logger, pkg_name, "Start cloning")

  (output, result) = execCmdEx("gbp clone -v " & git_url, options = {poStdErrToStdOut}, workingDir = work_dir)

  if result == 0:
    ulog_info(logger, pkg_name, output, show_verbose)
    ulog_info(logger, pkg_name, "Clone completed")
  else:
    ulog_error(logger, show_verbose, pkg_name, "Error while cloning", output)


proc ujobs_pull*(logger: LogHandler, pkg_name, work_dir: string, show_verbose: bool): int =
  var
    output: string
  ulog_info(logger, pkg_name, "Start pulling")

  (output, result) = execCmdEx("gbp pull -v", options = {poStdErrToStdOut}, workingDir = work_dir)

  if result == 0:
    ulog_info(logger, pkg_name, output, show_verbose)
    ulog_info(logger, pkg_name, "Pulling completed")
  else:
    ulog_error(logger, show_verbose, pkg_name, "Error while pulling", output)


proc ujobs_upgrade*(logger: LogHandler, pkg_name, work_dir: string, new_version: var string, show_verbose: bool): int =
  var
    output: string
    has_new_version: bool

  ulog_info(logger, pkg_name, "Checking new update")

  (output, result) = execCmdEx("gbp import-orig --no-interactive --pristine-tar --uscan -v", workingDir = work_dir)

  ulog_info(logger, pkg_name, output, show_verbose)

  if "gbp:info: package is up to date, nothing to do." in output:
    ulog_info(logger, pkg_name, "No updates found")
    has_new_version = false
  else:
    has_new_version = true
    for each_line in uutils_lines(output):
      if "Newest version of" in each_line:
        let
          jump = find(each_line, "is")
        new_version = captureBetween(each_line, ' ', ',', start=jump)
        ulog_info(logger, pkg_name, "Found new version: " & new_version)
        break

  if has_new_version:
    if result == 0:
      ulog_updated(logger, pkg_name, new_version)
    else:
      ulog_error(logger, show_verbose, pkg_name, "Error while upgrading version", output)
  # else: No update found showed above


proc ujobs_tag*(logger: LogHandler, pkg_name: string, work_dir: string, show_verbose: bool): int =
  var
    output: string

  ulog_info(logger, pkg_name, "Creating new tag")

  (output, result) = execCmdEx("gbp tag -v", options = {poStdErrToStdOut}, workingDir = work_dir)

  if result == 0:
    ulog_info(logger, pkg_name, output, show_verbose)
    ulog_info(logger, pkg_name, "New tag created")
  else:
    ulog_error(logger, show_verbose, pkg_name, "Error while creating new tag", output)


proc ujobs_changelog*(logger: LogHandler, pkg_name, work_dir, version: string, show_verbose: bool): int =
  var
    output: string

  ulog_info(logger, pkg_name, "Generating new changelog")
  # TODO use distribution and version name from config

  (output, result) = execCmdEx("gbp dch --distribution=stable --force-distribution -c -N " & version & "-1parrot1", options = {poStdErrToStdOut}, workingDir = work_dir)

  if result == 0:
    ulog_info(logger, pkg_name, output, show_verbose)
    ulog_info(logger, pkg_name, "New changelog created")
  else:
    ulog_error(logger, show_verbose, pkg_name, "Error while creating new changelog", output)


proc ujobs_push*(logger: LogHandler, pkg_name, work_dir: string, show_verbose: bool): int =
  var
    output: string

  ulog_info(logger, pkg_name, "Pushing new version to repo")

  (output, result) = execCmdEx("gbp push --pristine-tar -v", workingDir = work_dir)

  if result == 0:
    ulog_info(logger, pkg_name, output, show_verbose)
    ulog_info(logger, pkg_name, "Push completed")
  else:
    ulog_error(logger, show_verbose, pkg_name, "Error while pushing", output)
